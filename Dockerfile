FROM alpine:3.10
ARG terraform_version
ARG ansible_version
ARG tf_ansible=2.2.0

RUN apk add --no-cache \
    python \
	py-pip \
	openssl-dev \
	openssh-client \
	curl \
	git 
  
RUN apk add --no-cache --virtual build-deps \
    python-dev \
	build-base \
	libffi-dev \
  && rm -rf /var/cache/apk/* \
  && pip install ansible==${ansible_version} \
  && wget https://releases.hashicorp.com/terraform/${terraform_version}/terraform_${terraform_version}_linux_amd64.zip \
  && unzip terraform_${terraform_version}_linux_amd64.zip \
  && rm terraform_${terraform_version}_linux_amd64.zip \
  && mv terraform /usr/bin/ \
  && apk del build-deps

COPY files/install-tf-ansible.sh .
RUN OSTYPE=linux /bin/sh install-tf-ansible.sh -v ${tf_ansible}
